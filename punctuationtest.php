<?php
include('nlp/autoloader.php');
use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
$text = "Please allow me to introduce myself
I'm a man of wealth and taste";
$tok = new WhitespaceAndPunctuationTokenizer();
print_r($tok->tokenize($text));
// Array
// (
// [0] => Please
// [1] => allow
// [2] => me
// [3] => to
// [4] => introduce
// [5] => myself
// [6] => I
// [7] => '
// [8] => m
// [9] => a
// [10] => man
// [11] => of
// [12] => wealth
// [13] => and
// [14] => taste
// )

?>