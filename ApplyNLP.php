    <?php

    include('nlp/autoloader.php');

    use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
    use NlpTools\Stemmers\PorterStemmer;
    use NlpTools\Stemmers\LancasterStemmer;
    use NlpTools\Utils\StopWords;

    use NlpTools\Documents\TokensDocument;

    use NlpTools\Analysis\FreqDist;

    class ApplyNLP {



        public function process($string){

            $string=strtolower(preg_replace(
                array('#[\\s-]+#', '#[^A-Za-z0-9\. -]+#'),
                array(' ', ''),
                urldecode($string)));


            $tok = new WhitespaceAndPunctuationTokenizer();
            $array=array();
            $array=$tok->tokenize($string);


            $stopword_file=array();
            $stopword_file=preg_split("/\\r\\n|\\r|\\n/",file_get_contents('/var/www/nlp-test/nlp/src/NlpTools/Utils/stopwordlist.txt',true ));
            $stopword=new StopWords($stopword_file);


            $doc = new TokensDocument($array);
            $doc->applyTransformation($stopword);
            $array=$doc->getDocumentData();


            $stemmer=new  PorterStemmer();
            $array=($stemmer->stemAll($array));


            $stopword_file=preg_split("/\\r\\n|\\r|\\n/",file_get_contents('/var/www/nlp-test/nlp/src/NlpTools/Utils/stopwordlist.txt',true ));
            $stopword=new StopWords($stopword_file);


            $doc = new TokensDocument($array);
            $doc->applyTransformation($stopword);
            $array=$doc->getDocumentData();


            $frequency=new FreqDist($array);
            $frequency=($frequency->getKeyValues());

            return $frequency;


        }

        public function merge(array $a,array $b)
        {
            foreach($b as $k=>$v){
                if(isset($a[$k]))
                    $a[$k]+=$v;
                else
                    $a[$k]=$v;
            }

            return $a;
        }


    }
    ?>