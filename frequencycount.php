<?php
include('nlp/autoloader.php');

use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
use NlpTools\Stemmers\PorterStemmer;
use NlpTools\Stemmers\LancasterStemmer;
use NlpTools\Utils\StopWords;

use NlpTools\Documents\TokensDocument;

use NlpTools\Analysis\FreqDist;

/*echo  urldecode(file_get_contents('testtext.txt'));


$text = "Please allow me to transformer transform transforming myself
I'm a man of wealth wealthy tasteful and taste";*/


$text=strtolower(preg_replace(
    array('#[\\s-]+#', '#[^A-Za-z0-9\. -]+#'),
    array(' ', ''),
    urldecode(file_get_contents('testtext.txt'))));

$tok = new WhitespaceAndPunctuationTokenizer();
$array2=array();
$array2=$tok->tokenize($text);

/*print_r($array2);*/



$stopwordfile=array();
$stopwordfile=preg_split("/\\r\\n|\\r|\\n/",file_get_contents('/var/www/nlp-test/nlp/src/NlpTools/Utils/stopwordlist.txt',true ));
//print_r($stopwordfile);

$stopword=new StopWords($stopwordfile);


$doc = new TokensDocument($array2);
$doc->applyTransformation($stopword);
$array2=$doc->getDocumentData();

print_r($array2);
$array3=array();
$array3=$array2;

$stemmer=new  PorterStemmer();

$stemmer2=new LancasterStemmer();

$array2=($stemmer->stemAll($array2));
print_r($array2);

$array3=($stemmer2->stemAll($array3));
print_r($array3);

$frequency=new FreqDist($array2);
echo '\n\n\n\n';
$a=($frequency->getKeyValues());
$b=($frequency->getKeyValues());



foreach($b as $k=>$v){
    if(isset($a[$k]))
        $a[$k]+=$v;
    else
        $a[$k]=$v;
}

print_r($a);


?>